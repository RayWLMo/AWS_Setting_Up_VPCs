# Using Virtual Private Clouds (VPCs) in AWS

## Creating the VPC
- On the AWS search bar, type in VPC
- Alternatively, click on services, and scroll down to the heading `Networking and Content Delivery` where `VPC` should be under it.
- To create a new VPC, click the `Create VPC` in the top right
- Give the VPC a suitable name using the similar naming convention used in the past e.g., `eng89_yourname_vpc`
- For the IPv4 CIDR block - use the IP - `10.206.0.0/16`
- We can leave everything the same but optionally, a description tag can be added to tell others the purpose of the VPC

## Security Groups
- For security groups, there needs to be one for each machine (app/public, db/private and bastion)
- To create a new security group, click the `Security Groups` tab on the left sidebar and click `Create New Security Group` in the top right
- Add a suitable name (e.g., eng89_yourname_app_sg) and description (For use by app instance)
- Select the VPC that was just created
- (Optional)Add a name **tag** so that it can be easily seen in the list of security groups

### Security Group Inbound and Outbound Rules
- Next, the inbound and outbound rules of each security group needs to be defined. All of the inbound and outbound rules can also have a description added so it's easier to tell which rule does what e.g., SSH inbound rule allows only me to be able to SSH securely into the server.
- For some rules, another security group might need to be selected. The best approach for this would be to create all the security groups first then add the rules after.

#### Public / App Security Group
- Inbound


| Type     | Port Range | Source                        |
|--------: | :---------:| :-----------------------------|
| SSH      | 22         | YOUR.PERSONAL.IPv4.ADDRESS/32 |
| HTTP     | 80         | 0.0.0.0/0 |
- Outbound

| Type     | Port Range     | Destination |
| :------------- | :------------- | :--------- |
| All Traffic | All       | 0.0.0.0/0 |

#### Private / DB Security Group
- Inbound

| Type     | Port Range | Source    |
| :------- | :--------- | :------------|
| SSH      | 22         | BASTION.SECURITY.GROUP |
| Custom TCP | 27017    | APP.SECURITY.GROUP |
- Outbound

| Type     | Port Range     | Destination |
| :------------- | :------------- | :--------- |
| All Traffic | All       | 0.0.0.0/0 |

#### Bastion Security Group
- Inbound

| Type     | Port Range | Source    |
| :------- | :--------- | :------------|
| SSH      | 22         | YOUR.PERSONAL.IPv4.ADDRESS/32 |
- Outbound

| Type     | Port Range     | Destination |
| :------------- | :------------- | :--------- |
| All Traffic | All       | 0.0.0.0/0 |
| Custom TCP | 0 | DB.SECURITY.GROUP |

## Internet Gateway
- Once all the security groups are created, the internet gateway need to be made so that the VPC can communicate to the internet
- To create an internet gateway, find `Internet Gateways` on the left hand sidebar
- Click `Create Internet Gateway` in the top right
- Give the internet gateway a suitable name e.g., eng89_yourname_IG
- Attach to the VPC by clicking the prompt at the top once the internet gateway has been created
- Select the VPC created at the beginning and save the changes.

## Route Table
- Next, the route table for the VPC needs to be created
- Click `Route Tables` on the left hand sidebar and then `Create route table` found in the top right
- Give the route table a suitable name following the same naming convention e.g., eng89_yourname_rt
- Select the VPC created from the drop down menu and click `Create route table`
- Once the route table has been created, it now needs a route
- When in the `Routes` tab of your route table, click `Edit routes`
- Click `Add route` as the local route is needed
- For `Destination` - enter `0.0.0.0/0`, this is for the VPC to connect to the internet
- For `Target` - click `Internet Gateway` from the drop down menu and select the internet gateway created in the last step.

## Creating subnets within the VPC
- Now the subnets for each instance needs to be created
- Click `Subnets` in the left-hand sidebar and click `Create subnet`
- As there are 3 instances, a subnet needs to be created for each one
### App
- As the main instance is the app, it will be created first
- select the VPC created from the drop down menu or search if necessary
- Name the subnet using the naming convention e.g., eng89_yourname_app_subnet
- Select `eu-west-1a` for the `Availability Zone`
- For the `IPv4 CIDR block` enter `10.206.1.0/24` as it is a subsection of the `10.206.0.0/16` VPC IP address
- (Optional) Add a name tag so the subnet can be more easily located in the future
### DB
- Select Add new subnet
- Give the new subnet a suitable name and optional name tag
- The `Availability Zone` will be the same as the app subnet
- However for the `IPv4 CIDR block` it will `10.206.2.0/24` as the DB instance is on a different subnet
### Bastion
- Select Add new subnet
- Give the new subnet a suitable name and optional name tag
- The `Availability Zone` will be the same as the app and db subnet
- However for the `IPv4 CIDR block` it will `10.206.3.0/24` as the Bastion instance is on a different subnet

### Adding the route table to the subnets
- Now that the subnets have been created, some need to be associated with a route table so it can access the internet
- **This only applies to the `App` and the `Bastion` subnets**
- Select the app subnet from the `Subnets` page or search for it if needed.
- Under the `Route table` tab, click `Edit route table association`
- Open the drop down menu for `Route table ID` and select the route table created earlier and click `Save`

## NACL
- Now that the subnets are created, the network access lists (NACLs) need to be created and associated with the respective subnets
- Click `Network ACLs` from the left-hand sidebar and click `Create network ACL` in the top right
- Give the NACL a suitable name e.g., eng89_yourname_app_NACL or eng89_yourname_public_NACL
- Select the VPC created at the beginning and click `Create network ACL` to finish
- Repeat this for the private and bastion NACLs and name them appropriately

### Configuring the NACLs
- Now that the NACLs are created, they need to be configured to the appropriate subnet and have network rules set up
- On the main `Network ACLs` page, find the NACLs created either by scrolling or searching using the search bar

#### Changing the inbound + outbound rules
- First, the inbound + outbound rules will be configured
- Start with the app/public NACL and repeat with the other 2 NACLs using the configurations below
- Select `Inbound rules` or `Outbound rules` then click `Edit inbound/outbound rules` depending on which one is being configured
- There should be a default rule to deny all incoming traffic, all the rules that are being created are set to `Allow`

##### App / Public NACL
- Inbound

| Rule number   | Type     | Port Range | Source |
| :------------- | :------------- | :---- | :--- |
| 100       | HTTP (80)  | 80 | 0.0.0.0/0 |
| 110       | SSH (22) | 22 | YOUR.PERSONAL.IP.ADDRESS/32 |
| 120       | Custom TCP | 1024 - 65535 | 0.0.0.0/0 |
- Outbound

| Rule number   | Type     | Port Range | Destination |
| :------------- | :------------- | :---- | :--- |
| 100       | HTTP (80)  | 80 | 0.0.0.0/0 |
| 110       | Custom TCP | 27017 | 10.206.2.0/24 |
| 120       | Custom TCP | 1024 - 65535 | 0.0.0.0/0 |
| 130       | HTTPS (443) | 443 | 0.0.0.0/0 |

#### DB / Private NACL
- Inbound

| Rule number   | Type     | Port Range | Source |
| :------------- | :------------- | :---- | :--- |
| 100       | Custom TCP | 27017 | 10.206.1.0/24 |
| 110       | Custom TCP | 1024 - 65535 | 0.0.0.0/0 |
| 120       | SSH (22) | 22 | 10.206.3.0/24 |
- Outbound

| Rule number   | Type     | Port Range | Destination |
| :------------- | :------------- | :---- | :--- |
| 100       | HTTP (80)  | 80 | 0.0.0.0/0 |
| 110       | Custom TCP | 1024 - 65535 | 10.206.1.0/24 |
| 120       | Custom TCP | 1024 - 65535 | 10.206.3.0/24 |

#### Bastion NACL
- Inbound

| Rule number   | Type     | Port Range | Source |
| :------------- | :------------- | :---- | :--- |
| 100       | SSH (22) | 22 | YOUR.PERSONAL.IP.ADDRESS/32 |
| 110       | Custom TCP | 1024 - 65535 | 10.206.2.0/24 |
- Outbound

| Rule number   | Type     | Port Range | Destination |
| :------------- | :------------- | :---- | :--- |
| 100       | SSH (22) | 22 | 10.206.2.0/24 |
| 110       | Custom TCP | 1024 - 65535 | YOUR.PERSONAL.IP.ADDRESS/32 |

#### Subnet associations
- Once all the NACL rules are set up, they need to be associated with their respective subnet before the instances can be launched
- Select the app NACL and click on `Subnet associations`, then `Edit subnet associations`
- Select the app subnet created earlier and save the changes
- Repeat with the db and bastion NACLs with their respective subnets

## Launch EC2 Instances
- Now that everything is setup and configured, the instances can be launched, if a refresher is needed, find the basics of setting up instances [here](https://gitlab.com/RayWLMo/2_Tier_App_Deployment)
### Building the app and db instances
- As previously AMIs were created on the app and db instances, those can be used so that the installation steps are no longer needed such as installing nginx and node etc.
- When configuring the instance setup, instead of the default VPC, select the one created and select the appropriate subnet for that instance e.g., app/public subnet for the app instance
- During the configuration (Step 3) for the app, it needs a public IP so set it to enable and for the database, it doesn't so disable it.
- When configuring the security group, select the one created earlier from the drop down menu, double check the name to ensure that it is the correct one

### Configuring the Bastion server
- As there is no AMI for the bastion server, it needs to be created from scratch
- choose `Ubuntu Server 16.04 LTS (HVM), SSD Volume Type as the instance type
- Repeat the steps for the app/db instance ensuring the correct VPC, subnet and security group is selected before launching the instance
- The bastion will also need a public IP for access

## Accessing the db from bastion server
- Navigate to the `.ssh` folder in Git Bash
- Use this command to copy the access key into the bastion server - `scp -i eng89_devops.pem eng89_devops.pem ubuntu@bastion.IP.address:~/`
- SSH into the bastion server using the IP given by AWS
- Using the `mv eng89_devops.pem .ssh/` command, the pem file key is moved to the `.ssh` folder within the bastion server
- Enter the `.ssh` folder using `cd .ssh`
- Authorise the pem file key - `chmod 400 eng89_devops.pem`
- Now we can ssh into the db/private server through bastion using the private IP generated by AWS `ssh -i "eng89_devops.pem" ubuntu@db.private.IP`
